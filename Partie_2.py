import math
from fractions import Fraction


class Domino:

    def __init__(self, face_1, face_2):
        self.face_1 = face_1
        self.face_2 = face_2

    def affiche_points(self):
        print("face A: " + str(self.face_1) + ", face B: " + str(self.face_2))

    def total(self):
        return self.face_1 + self.face_2


class CompteBancaire:

    def __init__(self, name, sold=0):
        self.name = name
        self.sold = sold

    def affiche(self):
        print("Le solde du compte de " + str(self.name) + " est " + str(self.sold) + " euros. ")

    def depot(self, value):
        self.sold = self.sold + value

    def retrait(self, value):
        self.sold = self.sold - value


class TableMultiplication:

    def __init__(self, nbForTable, nb=0):
        self.nbForTable = [nbForTable * i for i in range(1, 10)]
        self.nb = nb

    def prochain(self):
        print(self.nbForTable[self.nb])
        self.nb = self.nb + 1


class Fractions:

    def __init__(self, num, denom):
        self.num = num
        self.denom = denom

    def __str__(self):
        return str(self.num) + "/" + str(self.denom)

    def printing(self):
        print(str(self.num) + "/" + str(self.denom))

    def __add__(self, other):
        num = str(Fraction(self.num, self.denom) + Fraction(other.num, other.denom)).split("/")[0]
        denom = str(Fraction(self.num, self.denom) + Fraction(other.num, other.denom)).split("/")[1]
        return Fractions(num, denom)

    def __truediv__(self, other):
        num = str(Fraction(self.num, self.denom) / Fraction(other.num, other.denom)).split("/")[0]
        denom = str(Fraction(self.num, self.denom) / Fraction(other.num, other.denom)).split("/")[1]
        return Fractions(num, denom)


"""
Exercice 5. Écrivez des méthodes spéciales .__repr__() adaptées pour les classes des exercices
précédents.
"""
print("\n\nExo_6")


class Poly:

    def __init__(self, polylist):
        if not isinstance(polylist, list):
            raise ValueError("Ceci n’est pas une liste" + str(polylist))
        self.polylist = polylist

    def addition(self, p1, p2):

        if len(p1) > len(p2):
            new = [0] * len(p1)
        else:
            new = [0] * len(p2)
        for i in range(len(p1)): new[i] += p1[i]
        for i in range(len(p2)): new[i] += p2[i]
        return

    def coeff(self):
        print(str(self.polylist))

    def evalue(self, nb):
        print(self.polylist[0] + (self.polylist[1] * nb) - (nb * 2 ** 2))

    def __add__(self, other):
        return Poly(self.addition(self.polylist, other.lpolynome))



print("\nExo_1")
d = Domino(4, 6)
d.affiche_points()
x = d.total()
print(x)

print("\n\nExo_2")
compte1 = CompteBancaire("Jean", 1000)
compte1.retrait(200)
compte1.affiche()
compte2 = CompteBancaire("Marc")
compte2.depot(500)
compte2.affiche()

print("\n\nExo_3")
tab = TableMultiplication(3)
tab.prochain()
tab.prochain()
tab.prochain()
tab.prochain()

print("\n\nExo_4")
f = Fractions(3, 4)
f.printing()
g = Fractions(1, 2)
g.printing()
r1 = f + g
r1.printing()
r2 = f / g
r2.printing()

print("\n\nExo_5")

print("\n\nExo_6")
p = Poly([3, 4, -2])
p.coeff()
p.evalue(2)
