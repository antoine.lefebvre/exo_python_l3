from itertools import chain, combinations


def nb_app(nb):
    def decorator(func):
        return func()
    nb += 1


nb_app = 1


# @nb_app(nb_app)
def chiffrePorteBohneur(nb):
    result = nb
    while result > 10:
        result = sum([int(i) ** 2 for i in [i for i in str(result)]])

    if result == 1:
        print("Porte bohneur")
    else:
        print("Pas porte bohneur")


def g2(var, nb):
    return [i for i in var] * nb


def powerset(seq):
    if len(seq) <= 1:
        yield seq
        yield []
    else:
        for item in powerset(seq[1:]):
            yield [seq[0]] + item
            yield item


if __name__ == '__main__':
    print("\nExo_1")
    chiffrePorteBohneur(913)

    print("\n\nExo_2")
    print(g2(["a", "b", "c", "d"], 3))

    print("\n\nExo_3")
    print([x for x in powerset([1, 2, 3])])

    print("\n\nExo_4")
    chiffrePorteBohneur(913)
    print(nb_app)
